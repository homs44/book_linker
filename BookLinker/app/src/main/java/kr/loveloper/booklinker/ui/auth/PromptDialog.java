package kr.loveloper.booklinker.ui.auth;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.webkit.WebSettings;
import android.webkit.WebView;

import kr.loveloper.booklinker.R;
import kr.loveloper.booklinker.custom.webViewClient.CodeReceiver;
import kr.loveloper.booklinker.custom.webViewClient.CustomWebViewClient;
import kr.loveloper.booklinker.databinding.DialogPromptBinding;

public class PromptDialog extends Dialog {

    private DialogPromptBinding binding;
    private CodeReceiver receiver;

    public PromptDialog(@NonNull Context context, CodeReceiver receiver) {
        super(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        this.receiver = receiver;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),R.layout.dialog_prompt,null,false);
        setContentView(binding.getRoot());
        WebView webView = binding.acPromptWebview;
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new CustomWebViewClient(new CodeReceiver() {
            @Override
            public void onReceived(String code) {
                receiver.onReceived(code);
                dismiss();
            }
        }));
        String uri = Uri.parse("https://kauth.kakao.com/oauth/authorize")
                .buildUpon()
                .appendQueryParameter("client_id", "59db5db1468bb82078e382899939bb7a")
                .appendQueryParameter("redirect_uri", "http://booklinker-env-1.azvupf7mgf.ap-northeast-2.elasticbeanstalk.com/oauth")
                .appendQueryParameter("response_type", "code")
                .build().toString();

        webView.loadUrl(uri);
    }


}
