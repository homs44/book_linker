package kr.loveloper.booklinker.ui.main;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kr.loveloper.booklinker.R;
import kr.loveloper.booklinker.databinding.ItemBookBinding;
import kr.loveloper.booklinker.model.book.Book;

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.BookViewHolder> {

    private ArrayList<Book> list;

    public BookListAdapter() {
        this.list = new ArrayList<>();
    }

    public void append(ArrayList<Book> list) {
        int size = this.list.size();
        this.list.addAll(list);
        if (size == 0) {
            notifyDataSetChanged();
        } else {
            notifyItemRangeChanged(size, list.size());
        }
    }

    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemBookBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_book, parent, false);
        return new BookViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BookViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public static class BookViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private ItemBookBinding binding;
        public BookViewHolder(ItemBookBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bind(Book book){
            binding.setBook(book);
            Picasso.get().load(book.getThumbnail()).into(binding.iBookThumbnail);
        }
    }

}
