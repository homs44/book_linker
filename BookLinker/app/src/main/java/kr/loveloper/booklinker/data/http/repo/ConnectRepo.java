package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class ConnectRepo {
    @SerializedName("id")
    public int id;
    public ConnectRepo(int id) {
        this.id = id;
    }
}
