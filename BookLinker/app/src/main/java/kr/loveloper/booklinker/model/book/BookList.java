package kr.loveloper.booklinker.model.book;

import java.util.ArrayList;

public class BookList {
    private Meta meta;
    private ArrayList<Book> books;

    public BookList(Meta meta, ArrayList<Book> books) {
        this.meta = meta;
        this.books = books;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

}
