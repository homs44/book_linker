package kr.loveloper.booklinker.service;

import android.app.Application;

import io.reactivex.Observable;
import kr.loveloper.booklinker.data.http.KakaoSearchService;
import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.book.BookList;
import kr.loveloper.booklinker.service.command.BookListCommand;

public class BookService {
    private KakaoSearchService kakaoBookService;

    public BookService() {
        this.kakaoBookService = new KakaoSearchService();
    }

    public Observable<BookList> search(BookListCommand bookListCommand) {
        return this.kakaoBookService.searchBook(
                bookListCommand.getQuery(),
                bookListCommand.getSort(),
                bookListCommand.getPage(),
                bookListCommand.getSize(),
                bookListCommand.getTarget());
    }
}
