package kr.loveloper.booklinker.ui.main;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.loveloper.booklinker.MyApplication;
import kr.loveloper.booklinker.R;
import kr.loveloper.booklinker.databinding.ActivityContainerBinding;
import kr.loveloper.booklinker.ui.auth.AuthViewModel;

public class MainFragment extends Fragment {

    MainViewModel viewModel;
    private Context context;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    public MainFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return viewModel.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewModel = new MainViewModel((Activity)context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewModel.onDetach();
    }

}
