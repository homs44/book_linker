package kr.loveloper.booklinker.model.book;

public class Meta {
    private boolean isEnd;
    private int pageableCount;
    private int totalCount;

    public Meta(boolean isEnd, int pageableCount, int totalCount) {
        this.isEnd = isEnd;
        this.pageableCount = pageableCount;
        this.totalCount = totalCount;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public int getPageableCount() {
        return pageableCount;
    }

    public void setPageableCount(int pageableCount) {
        this.pageableCount = pageableCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
