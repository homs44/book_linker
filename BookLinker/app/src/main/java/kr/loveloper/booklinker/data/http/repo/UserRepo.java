package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class UserRepo {
    @SerializedName("id")
    public String id;
    @SerializedName("properties")
    public PropertiesRepo properties;
    @SerializedName("kakao_account")
    public KakaoAcountRepo kakaoAccount;

    @Override
    public String toString() {
        return "UserRepo{" +
                "id='" + id + '\'' +
                ", properties=" + properties +
                ", kakaoAccount=" + kakaoAccount +
                '}';
    }
}
