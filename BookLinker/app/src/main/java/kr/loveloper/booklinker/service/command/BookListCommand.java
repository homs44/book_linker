package kr.loveloper.booklinker.service.command;

public class BookListCommand {
    private String query;
    private String sort;
    private int page;
    private int size;
    private String target;

    public BookListCommand() {
        this.init();
    }

    public void init() {
        this.query = null;
        this.sort = null;
        this.page = 1;
        this.size = 10;
        this.target = null;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
