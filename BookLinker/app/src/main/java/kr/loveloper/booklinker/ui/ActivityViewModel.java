package kr.loveloper.booklinker.ui;

import android.content.Intent;
import android.os.Bundle;

public abstract class ActivityViewModel {
    public void onCreate(Bundle savedInstanceState) {

    }

    public void onPause() {

    }

    public void onResume() {

    }

    public void onDestroy() {

    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return false;
    }
}
