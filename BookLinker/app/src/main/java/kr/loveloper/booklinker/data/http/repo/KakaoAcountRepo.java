package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class KakaoAcountRepo {

    @SerializedName("has_email")
    public boolean hasEmail;
    @SerializedName("is_email_valid")
    public boolean isEmailValid;
    @SerializedName("is_email_verified")
    public boolean isEmailVerified;
    @SerializedName("email")
    public String email;
    @SerializedName("has_age_range")
    public boolean hasAgeRange;
    @SerializedName("age_range")
    public String ageRange;
    @SerializedName("has_birthday")
    public boolean hasBirthday;
    @SerializedName("birthday")
    public String birthday;
    @SerializedName("has_gender")
    public boolean hasGender;
    @SerializedName("gender")
    public String gender;

    @Override
    public String toString() {
        return "KakaoAcountRepo{" +
                "hasEmail=" + hasEmail +
                ", isEmailValid=" + isEmailValid +
                ", isEmailVerified=" + isEmailVerified +
                ", email='" + email + '\'' +
                ", hasAgeRange=" + hasAgeRange +
                ", ageRange='" + ageRange + '\'' +
                ", hasBirthday=" + hasBirthday +
                ", birthday='" + birthday + '\'' +
                ", hasGender=" + hasGender +
                ", gender='" + gender + '\'' +
                '}';
    }
}
