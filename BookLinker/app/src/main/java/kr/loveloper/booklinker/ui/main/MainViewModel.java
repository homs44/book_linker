package kr.loveloper.booklinker.ui.main;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kr.loveloper.booklinker.MyApplication;
import kr.loveloper.booklinker.R;
import kr.loveloper.booklinker.databinding.FragmentMainBinding;
import kr.loveloper.booklinker.model.book.BookList;
import kr.loveloper.booklinker.service.command.BookListCommand;
import kr.loveloper.booklinker.ui.FragmentViewModel;
import kr.loveloper.booklinker.ui.container.ContainerActivity;
import kr.loveloper.booklinker.util.ViewUtil;

public class MainViewModel extends FragmentViewModel {
    private FragmentMainBinding binding;
    private Activity activity;
    private CompositeDisposable compositeDisposable;
    private MyApplication myApplication;
    private BookListAdapter bookListAdapter;

    private BookListCommand bookListCommand;

    MainViewModel(Activity activity) {
        this.activity = activity;
        this.myApplication = (MyApplication)activity.getApplication();
        this.compositeDisposable = new CompositeDisposable();
        this.bookListCommand = new BookListCommand();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, null, true);
        bookListAdapter = new BookListAdapter();
        binding.fMainRv.setAdapter(bookListAdapter);
        binding.fMainRv.setLayoutManager(new LinearLayoutManager(this.activity,LinearLayoutManager.VERTICAL,false));
        binding.fMainEt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String keyword = binding.fMainEt.getText().toString();
                    ViewUtil.hideKeyboard(activity);
                    search(keyword);
                    return true;
                }
                return false;
            }
        });
        return this.binding.getRoot();
    }

    @Override
    public void onDetach() {
        this.compositeDisposable.clear();
    }

    public void search(String str) {
        bookListCommand.setQuery(str);
        this.compositeDisposable.add(
                myApplication.getBookService().search(bookListCommand)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<BookList>() {
                    @Override
                    public void accept(BookList bookList) throws Exception {
                        bookListAdapter.append(bookList.getBooks());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                }));
    }


}
