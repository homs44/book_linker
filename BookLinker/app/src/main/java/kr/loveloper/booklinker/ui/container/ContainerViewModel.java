package kr.loveloper.booklinker.ui.container;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.squareup.otto.Subscribe;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kr.loveloper.booklinker.C;
import kr.loveloper.booklinker.MyApplication;
import kr.loveloper.booklinker.R;
import kr.loveloper.booklinker.databinding.ActivityContainerBinding;
import kr.loveloper.booklinker.event.BusProvider;
import kr.loveloper.booklinker.model.token.Token;
import kr.loveloper.booklinker.ui.ActivityViewModel;
import kr.loveloper.booklinker.ui.auth.AuthFragment;
import kr.loveloper.booklinker.ui.main.MainFragment;

public class ContainerViewModel extends ActivityViewModel {

    private ActivityContainerBinding binding;
    private FragmentActivity activity;
    private MyApplication myApplication;
    private CompositeDisposable compositeDisposable;


    public ContainerViewModel(FragmentActivity activity) {
        this.activity = activity;
        this.compositeDisposable = new CompositeDisposable();
        this.myApplication = (MyApplication)activity.getApplication();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.binding = DataBindingUtil.setContentView(activity, R.layout.activity_container);
        BusProvider.getInstance().register(this);

        Token token = myApplication.getTokenService().getTokenFromLocalDatabase();
        if(token == null){
            showAuthFragment();
        }else{
            compositeDisposable.add(
            myApplication.getUserService().connect()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<Integer>() {
                        @Override
                        public void accept(Integer integer) throws Exception {
                            showMainFragment();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            throwable.printStackTrace();
                        }
                    }));
            showMainFragment();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        this.compositeDisposable.clear();
    }

    private void showAuthFragment() {
        FragmentManager fm = this.activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(binding.container.getId(), AuthFragment.newInstance());
        fragmentTransaction.commit();
    }

    private void showMainFragment() {
        FragmentManager fm = this.activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(binding.container.getId(), MainFragment.newInstance());
        fragmentTransaction.commit();
    }

    @Subscribe
    public void onReceivedEvent(String action) {
        if(C.ACTION_GO_MAIN.equals(action)){
            showMainFragment();
        }

    }

}
