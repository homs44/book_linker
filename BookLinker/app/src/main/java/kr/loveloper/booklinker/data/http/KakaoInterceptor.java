package kr.loveloper.booklinker.data.http;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.token.Token;
import okhttp3.Interceptor;
import okhttp3.Request;

public class KakaoInterceptor implements Interceptor {
    @Override
    public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder().addHeader("Authorization", "KakaoAK " + "59db5db1468bb82078e382899939bb7a").build();
        return chain.proceed(newRequest);
    }
}
