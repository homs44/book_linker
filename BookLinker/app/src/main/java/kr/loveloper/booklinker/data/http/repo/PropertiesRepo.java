package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class PropertiesRepo {

    @SerializedName("nickname")
    public String nickname;
    @SerializedName("thumbnail_image")
    public boolean thumbnaleImage;
    @SerializedName("profile_image")
    public boolean profileImage;

    @Override
    public String toString() {
        return "PropertiesRepo{" +
                "nickname='" + nickname + '\'' +
                ", thumbnaleImage=" + thumbnaleImage +
                ", profileImage=" + profileImage +
                '}';
    }
}
