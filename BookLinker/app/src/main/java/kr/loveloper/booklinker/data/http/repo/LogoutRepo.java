package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class LogoutRepo {
    @SerializedName("id")
    public String id;

    @Override
    public String toString() {
        return "LogoutRepo{" +
                "id='" + id + '\'' +
                '}';
    }
}
