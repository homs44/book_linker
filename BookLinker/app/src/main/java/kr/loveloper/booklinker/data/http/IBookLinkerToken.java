package kr.loveloper.booklinker.data.http;

import io.reactivex.Observable;
import kr.loveloper.booklinker.data.http.repo.TokenRepo;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IBookLinkerToken {
    @GET("oauth")
    Observable<TokenRepo> oauth(@Query("code") String code);
}