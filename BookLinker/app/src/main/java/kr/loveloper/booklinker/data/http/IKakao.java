package kr.loveloper.booklinker.data.http;

import io.reactivex.Observable;
import kr.loveloper.booklinker.data.http.repo.ConnectRepo;
import kr.loveloper.booklinker.data.http.repo.LogoutRepo;
import kr.loveloper.booklinker.data.http.repo.SendToMeRepo;
import kr.loveloper.booklinker.data.http.repo.TokenRepo;
import kr.loveloper.booklinker.data.http.repo.UserRepo;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IKakao {


    @GET("/v1/user/signup")
    Observable<ConnectRepo> connect();

    @Headers({
            "Content-type: application/x-www-form-urlencoded;charset=utf-8"
    })
    @POST("oauth/token")
    Observable<TokenRepo> refreshToken(@Body String grantType, @Body String clientId, @Body String refreshToken);

    @GET("v2/user/me")
    Observable<UserRepo> me();

    @POST("v2/api/talk/memo/default/send")
    Observable<SendToMeRepo> sendToMe(@Body String templateObject);

    @GET("v1/user/logout")
    Observable<LogoutRepo> logout();


}