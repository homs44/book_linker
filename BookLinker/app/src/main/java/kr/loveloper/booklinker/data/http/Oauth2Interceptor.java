package kr.loveloper.booklinker.data.http;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.token.Token;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.internal.http2.Header;

public class Oauth2Interceptor implements Interceptor {
    private LocalDatabaseService localDatabaseService;
    public Oauth2Interceptor(LocalDatabaseService localDatabaseService){
        this.localDatabaseService = localDatabaseService;
    }

    @Override
    public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
        Token token = localDatabaseService.getToken();
        Request newRequest;
        if (token != null ) {
            Log.d("ingyu","Bearer " + token.getAccessToken());
            newRequest = chain.request().newBuilder().addHeader("Authorization", "KakaoAK " + token.getAccessToken()).build();
        } else newRequest = chain.request();

        Log.d("ingyu", "header : " + newRequest.headers().get("Authorization"));
        return chain.proceed(newRequest);
    }
}
