package kr.loveloper.booklinker.model.user;

public class Properties {
    private String nickname;
    private boolean thumbnaleImage;
    private boolean profileImage;

    public Properties(String nickname, boolean thumbnaleImage, boolean profileImage) {
        this.nickname = nickname;
        this.thumbnaleImage = thumbnaleImage;
        this.profileImage = profileImage;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public boolean isThumbnaleImage() {
        return thumbnaleImage;
    }

    public void setThumbnaleImage(boolean thumbnaleImage) {
        this.thumbnaleImage = thumbnaleImage;
    }

    public boolean isProfileImage() {
        return profileImage;
    }

    public void setProfileImage(boolean profileImage) {
        this.profileImage = profileImage;
    }
}
