package kr.loveloper.booklinker.data.http;


import io.reactivex.Observable;
import kr.loveloper.booklinker.data.http.repo.BookListRepo;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IKakaoSearch {

    @GET("v3/search/book")
    Observable<BookListRepo> searchBook(@Query("query") String query,
                                        @Query("sort") String sort,
                                        @Query("page") int page,
                                        @Query("size") int size,
                                        @Query("target") String target);

}