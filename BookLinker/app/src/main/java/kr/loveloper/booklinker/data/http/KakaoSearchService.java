package kr.loveloper.booklinker.data.http;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import kr.loveloper.booklinker.data.http.repo.BookListRepo;
import kr.loveloper.booklinker.data.http.repo.BookRepo;
import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.book.Book;
import kr.loveloper.booklinker.model.book.BookList;
import kr.loveloper.booklinker.model.book.Meta;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KakaoSearchService {
    private IKakaoSearch service;

    public KakaoSearchService() {
        Gson gson = new GsonBuilder()
                .setDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSXXX")
                .create();

        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new KakaoInterceptor()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https:/dapi.kakao.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        service = retrofit.create(IKakaoSearch.class);
    }

    public Observable<BookList> searchBook(String query, String sort, int page, int size, String target) {
        return this.service.searchBook(query, sort, page, size, target).map(new Function<BookListRepo, BookList>() {
            @Override
            public BookList apply(BookListRepo bookListRepo) throws Exception {
                ArrayList<Book> books = new ArrayList<>();
                for (BookRepo bookRepo : bookListRepo.books) {
                    Log.d("ingyu", bookRepo.toString());
                    books.add(new Book(
                            bookRepo.title,
                            bookRepo.contents,
                            bookRepo.url,
                            bookRepo.isbn,
                            bookRepo.datetime,
                            bookRepo.authors,
                            bookRepo.publisher,
                            bookRepo.translators,
                            bookRepo.price,
                            bookRepo.salePrice,
                            bookRepo.thumbnail,
                            bookRepo.status
                    ));
                }

                Meta meta = new Meta(
                        bookListRepo.meta.isEnd,
                        bookListRepo.meta.pageableCount,
                        bookListRepo.meta.totalCount
                );

                BookList bookList = new BookList(meta, books);

                return bookList;
            }
        });
    }

}
