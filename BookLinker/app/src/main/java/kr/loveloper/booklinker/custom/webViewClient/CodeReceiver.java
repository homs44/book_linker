package kr.loveloper.booklinker.custom.webViewClient;

public interface CodeReceiver {
    void onReceived(String code);
}
