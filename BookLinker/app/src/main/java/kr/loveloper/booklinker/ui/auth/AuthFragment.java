package kr.loveloper.booklinker.ui.auth;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.reactivex.subjects.PublishSubject;
import kr.loveloper.booklinker.MyApplication;

public class AuthFragment extends Fragment {

    private AuthViewModel viewModel;

    public static AuthFragment newInstance() {
        AuthFragment fragment = new AuthFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return viewModel.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.viewModel = new AuthViewModel((Activity)context);
        this.viewModel.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.viewModel.onDetach();
    }



}
