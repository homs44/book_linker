package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class MetaRepo {
    @SerializedName("is_end")
    public boolean isEnd;
    @SerializedName("pageable_count")
    public int pageableCount;
    @SerializedName("total_count")
    public int totalCount;

    @Override
    public String toString() {
        return "MetaRepo{" +
                "isEnd=" + isEnd +
                ", pageableCount=" + pageableCount +
                ", totalCount=" + totalCount +
                '}';
    }
}
