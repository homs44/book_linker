package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class BookRepo {
    @SerializedName("title")
    public String title;
    @SerializedName("contents")
    public String contents;
    @SerializedName("url")
    public String url;
    @SerializedName("isbn")
    public String isbn;
    @SerializedName("datetime")
    public String datetime;
    @SerializedName("authors")
    public String[] authors;
    @SerializedName("publisher")
    public String publisher;
    @SerializedName("translators")
    public String[] translators;
    @SerializedName("price")
    public int price;
    @SerializedName("sale_price")
    public int salePrice;
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("status")
    public String status;

    @Override
    public String toString() {
        return "BookRepo{" +
                "title='" + title + '\'' +
                ", contents='" + contents + '\'' +
                ", url='" + url + '\'' +
                ", isbn='" + isbn + '\'' +
                ", datetime='" + datetime + '\'' +
                ", authors=" + Arrays.toString(authors) +
                ", publisher='" + publisher + '\'' +
                ", translators=" + Arrays.toString(translators) +
                ", price=" + price +
                ", salePrice=" + salePrice +
                ", thumbnail='" + thumbnail + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
