package kr.loveloper.booklinker.model.token;

public class Token {
    private String accessToken;
    private String tokenType;
    private String refreshToken;
    private long expriesIn;
    private String scope;

    public Token(String accessToken, String tokenType, String refreshToken, long expriesIn, String scope) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.expriesIn = expriesIn;
        this.scope = scope;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpriesIn() {
        return expriesIn;
    }

    public void setExpriesIn(int expriesIn) {
        this.expriesIn = expriesIn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void refresh() {

    }

    @Override
    public String toString() {
        return "Token{" +
                "accessToken='" + accessToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", expriesIn=" + expriesIn +
                ", scope='" + scope + '\'' +
                '}';
    }
}
