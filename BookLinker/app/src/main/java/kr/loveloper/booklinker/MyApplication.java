package kr.loveloper.booklinker;

import android.app.Application;

import kr.loveloper.booklinker.service.BookService;
import kr.loveloper.booklinker.service.TokenService;
import kr.loveloper.booklinker.service.UserService;

public class MyApplication extends Application {

    private BookService bookService;
    private TokenService tokenService;
    private UserService userService;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public BookService getBookService() {
        if (bookService == null)
            bookService = new BookService();
        return bookService;
    }

    public TokenService getTokenService() {
        if (tokenService == null)
            tokenService = new TokenService(this);
        return tokenService;
    }

    public UserService getUserService() {
        if(userService == null)
            userService = new UserService(this);
        return userService;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
