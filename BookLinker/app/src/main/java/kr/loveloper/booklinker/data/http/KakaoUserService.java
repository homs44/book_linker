package kr.loveloper.booklinker.data.http;

import android.app.Application;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import kr.loveloper.booklinker.data.http.repo.ConnectRepo;
import kr.loveloper.booklinker.data.http.repo.LogoutRepo;
import kr.loveloper.booklinker.data.http.repo.SendToMeRepo;
import kr.loveloper.booklinker.data.http.repo.TokenRepo;
import kr.loveloper.booklinker.data.http.repo.UserRepo;
import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.token.Token;
import kr.loveloper.booklinker.model.user.KakaoAccount;
import kr.loveloper.booklinker.model.user.Properties;
import kr.loveloper.booklinker.model.user.User;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KakaoUserService {
    private IKakao service;

    public KakaoUserService(LocalDatabaseService localDatabaseService) {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new Oauth2Interceptor(localDatabaseService)).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https:/kapi.kakao.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        this.service = retrofit.create(IKakao.class);
    }

    public Observable<Integer> connect() {
        return this.service.connect().map(new Function<ConnectRepo, Integer>() {
            @Override
            public Integer apply(ConnectRepo connectRepo) throws Exception {
                return connectRepo.id;
            }
        });
    }

    public Observable<User> getUser() {
        return this.service.me().map(new Function<UserRepo, User>() {
            @Override
            public User apply(UserRepo userRepo) throws Exception {
                KakaoAccount kakaoAccount = new KakaoAccount(
                        userRepo.kakaoAccount.hasEmail,
                        userRepo.kakaoAccount.isEmailValid,
                        userRepo.kakaoAccount.isEmailVerified,
                        userRepo.kakaoAccount.email,
                        userRepo.kakaoAccount.hasAgeRange,
                        userRepo.kakaoAccount.ageRange,
                        userRepo.kakaoAccount.hasBirthday,
                        userRepo.kakaoAccount.birthday,
                        userRepo.kakaoAccount.hasGender,
                        userRepo.kakaoAccount.gender);

                Properties properties = new Properties(
                        userRepo.properties.nickname,
                        userRepo.properties.thumbnaleImage,
                        userRepo.properties.profileImage
                );
                String id = userRepo.id;
                return new User(id, properties, kakaoAccount);
            }
        });
    }

    public Observable<Token> refreshToken(String clientId, String refreshToken) {
        String grantType = "refresh_token";
        return this.service.refreshToken(grantType, clientId, refreshToken).map(new Function<TokenRepo, Token>() {
            @Override
            public Token apply(TokenRepo tokenRepo) throws Exception {
                Token token = new Token(
                        tokenRepo.accessToken,
                        tokenRepo.tokenType,
                        tokenRepo.refreshToken,
                        tokenRepo.expiresIn,
                        tokenRepo.scope);
                return token;
            }
        });
    }

    public Observable<String> logout() {
        return this.service.logout().map(new Function<LogoutRepo, String>() {
            @Override
            public String apply(LogoutRepo logoutRepo) throws Exception {
                return logoutRepo.id;
            }
        });
    }


    public Observable<Boolean> sendToMe( String templateObject) {
        return this.service.sendToMe( templateObject).map(new Function<SendToMeRepo, Boolean>() {
            @Override
            public Boolean apply(SendToMeRepo commandRepo) throws Exception {
                return commandRepo.resultCode == 200;
            }
        });
    }

}
