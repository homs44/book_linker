package kr.loveloper.booklinker.data.http;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import kr.loveloper.booklinker.data.http.repo.TokenRepo;
import kr.loveloper.booklinker.model.token.Token;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookLinkerService {
    private IBookLinkerToken service;

    public BookLinkerService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(" http://booklinker-env-1.azvupf7mgf.ap-northeast-2.elasticbeanstalk.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        service = retrofit.create(IBookLinkerToken.class);
    }

    public Observable<Token> getToken(String code){
        return this.service.oauth(code).map(new Function<TokenRepo, Token>() {
            @Override
            public Token apply(TokenRepo tokenRepo) throws Exception {
                Token token = new Token(
                        tokenRepo.accessToken,
                        tokenRepo.tokenType,
                        tokenRepo.refreshToken,
                        tokenRepo.expiresIn,
                        tokenRepo.scope);
                return token;
            }
        });
    }

}
