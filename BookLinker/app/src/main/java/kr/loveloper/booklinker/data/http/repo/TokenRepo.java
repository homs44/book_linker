package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class TokenRepo {

    @SerializedName("access_token")
    public String accessToken;

    @SerializedName("token_type")
    public String tokenType;

    @SerializedName("refresh_token")
    public String refreshToken;

    @SerializedName("expires_in")
    public long expiresIn;

    @SerializedName("scope")
    public String scope;

    @Override
    public String toString() {
        return "TokenRepo{" +
                "accessToken='" + accessToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", expiresIn=" + expiresIn +
                ", scope='" + scope + '\'' +
                '}';
    }
}
