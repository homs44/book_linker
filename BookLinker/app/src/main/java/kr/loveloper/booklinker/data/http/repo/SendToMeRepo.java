package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

public class SendToMeRepo {
    @SerializedName("result_code")
    public int resultCode;

    public SendToMeRepo(int resultCode) {
        this.resultCode = resultCode;
    }
}
