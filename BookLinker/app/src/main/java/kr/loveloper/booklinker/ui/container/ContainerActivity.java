package kr.loveloper.booklinker.ui.container;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.squareup.otto.Bus;

import kr.loveloper.booklinker.event.BusProvider;

public class ContainerActivity extends FragmentActivity {

    private ContainerViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ContainerViewModel(this);
        viewModel.onCreate(savedInstanceState);
    }


}
