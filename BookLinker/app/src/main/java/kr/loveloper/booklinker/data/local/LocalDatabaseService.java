package kr.loveloper.booklinker.data.local;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import kr.loveloper.booklinker.model.token.Token;

public class LocalDatabaseService {

    private SharedPreferences pref;
    private Application application;
    private static final String BOOKLINKER_DATABASE = "BOOKLINKER_DATABASE";

    private static final String ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY";
    private static final String REFRESH_TOKEN_KEY = "REFRESH_TOKEN_KEY";
    private static final String TOKEN_TYPE = "TOKEN_TYPE";
    private static final String EXPIRES_IN = "EXPIRES_IN";
    private static final String SCOPE = "SCOPE";


    public LocalDatabaseService(Application application) {
        this.application = application;
        this.pref = application.getSharedPreferences(BOOKLINKER_DATABASE, Context.MODE_PRIVATE);
    }

    public Token getToken() {
        String accessToken = pref.getString(ACCESS_TOKEN_KEY, null);
        String refreshToken = pref.getString(REFRESH_TOKEN_KEY, null);
        String tokenType = pref.getString(TOKEN_TYPE, null);
        long expiresIn = pref.getLong(EXPIRES_IN, 0);
        String scope = pref.getString(SCOPE, null);
        if(accessToken==null)return null;
        return new Token(
                accessToken,
                tokenType,
                refreshToken,
                expiresIn,
                scope);
    }

    public void saveToken(Token token){
        SharedPreferences sharedPref = application.getSharedPreferences(BOOKLINKER_DATABASE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ACCESS_TOKEN_KEY, token.getAccessToken());
        editor.putString(REFRESH_TOKEN_KEY, token.getRefreshToken());
        editor.putString(TOKEN_TYPE, token.getTokenType());
        editor.putLong(EXPIRES_IN, token.getExpriesIn());
        editor.putString(SCOPE, token.getScope());
        editor.apply();
    }
}
