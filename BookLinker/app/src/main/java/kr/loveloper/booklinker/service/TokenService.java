package kr.loveloper.booklinker.service;

import android.app.Application;

import io.reactivex.Observable;
import kr.loveloper.booklinker.data.http.BookLinkerService;
import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.token.Token;

public class TokenService {

    private BookLinkerService bookLinkerService;
    private LocalDatabaseService localDatabaseService;

    public TokenService(Application application) {
        this.bookLinkerService = new BookLinkerService();
        this.localDatabaseService = new LocalDatabaseService(application);
    }

    public Observable<Token> getToken(String code) {
        return this.bookLinkerService.getToken(code);
    }

    public Token getTokenFromLocalDatabase() {
        return this.localDatabaseService.getToken();
    }

    public void saveTokenToLocalDatabase(Token token) {
        this.localDatabaseService.saveToken(token);
    }

}
