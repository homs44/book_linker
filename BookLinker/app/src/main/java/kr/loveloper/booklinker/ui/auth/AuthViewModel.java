package kr.loveloper.booklinker.ui.auth;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kr.loveloper.booklinker.C;
import kr.loveloper.booklinker.MyApplication;
import kr.loveloper.booklinker.R;
import kr.loveloper.booklinker.custom.webViewClient.CodeReceiver;
import kr.loveloper.booklinker.databinding.FragmentAuthBinding;
import kr.loveloper.booklinker.event.BusProvider;
import kr.loveloper.booklinker.model.token.Token;
import kr.loveloper.booklinker.ui.FragmentViewModel;

public class AuthViewModel extends FragmentViewModel {
    private FragmentAuthBinding binding;
    private CompositeDisposable compositeDisposable;
    private Context context;
    private MyApplication myApplication;

    AuthViewModel(Activity activity) {
        this.compositeDisposable = new CompositeDisposable();
        this.myApplication = (MyApplication)activity.getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, null, true);
        this.binding.setViewModel(this);
        return this.binding.getRoot();
    }


    @Override
    public void onDetach() {
        compositeDisposable.clear();
    }


    @Override
    public void onAttach(Context context) {
        this.context = context;
    }

    private void saveToken(Token token) {
        myApplication.getTokenService().saveTokenToLocalDatabase(token);
    }

    public void loginWithKakao(View v) {
        new PromptDialog(context, new CodeReceiver() {
            @Override
            public void onReceived(String code) {
                getAccessToken(code);
            }
        }).show();
    }

    private void getAccessToken(String code) {
        compositeDisposable.add(
                myApplication.getTokenService().getToken(code)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Token>() {
                            @Override
                            public void accept(Token token) throws Exception {
                                saveToken(token);
                                BusProvider.getInstance().post(C.ACTION_GO_MAIN);
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                throwable.printStackTrace();
                            }
                        }));
    }


}
