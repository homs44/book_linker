package kr.loveloper.booklinker.service;

import android.app.Application;

import io.reactivex.Observable;
import kr.loveloper.booklinker.data.http.KakaoUserService;
import kr.loveloper.booklinker.data.local.LocalDatabaseService;
import kr.loveloper.booklinker.model.token.Token;
import kr.loveloper.booklinker.model.user.User;

public class UserService {
    private KakaoUserService kakaoUserService;

    public UserService(Application application) {
        LocalDatabaseService localDatabaseService = new LocalDatabaseService(application);
        this.kakaoUserService = new KakaoUserService(localDatabaseService);
    }

    public Observable<Integer> connect(){
        return kakaoUserService.connect();
    }

    public Observable<User> getUser() {
        return kakaoUserService.getUser();
    }

    public Observable<Boolean> sendToMe(String templateObject) {
        return kakaoUserService.sendToMe(templateObject);
    }

    public Observable<String> logout() {
        return kakaoUserService.logout();
    }

    public Observable<Token> refresh(String clientId,String refreshToken) {
        return kakaoUserService.refreshToken(clientId, refreshToken);
    }

}
