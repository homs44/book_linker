package kr.loveloper.booklinker.model.user;

public class KakaoAccount {
    private boolean hasEmail;
    private boolean isEmailValid;
    private boolean isEmailVerified;
    private String email;
    private boolean hasAgeRange;
    private String ageRange;
    private boolean hasBirthday;
    private String birthday;
    private boolean hasGender;
    private String gender;



    public KakaoAccount(boolean hasEmail, boolean isEmailValid, boolean isEmailVerified,
                        String email, boolean hasAgeRange, String ageRange, boolean hasBirthday,
                        String birthday, boolean hasGender, String gender) {
        this.hasEmail = hasEmail;
        this.isEmailValid = isEmailValid;
        this.isEmailVerified = isEmailVerified;
        this.email = email;
        this.hasAgeRange = hasAgeRange;
        this.ageRange = ageRange;
        this.hasBirthday = hasBirthday;
        this.birthday = birthday;
        this.hasGender = hasGender;
        this.gender = gender;
    }

    public boolean isHasEmail() {
        return hasEmail;
    }

    public void setHasEmail(boolean hasEmail) {
        this.hasEmail = hasEmail;
    }

    public boolean isEmailValid() {
        return isEmailValid;
    }

    public void setEmailValid(boolean emailValid) {
        isEmailValid = emailValid;
    }

    public boolean isEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        isEmailVerified = emailVerified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isHasAgeRange() {
        return hasAgeRange;
    }

    public void setHasAgeRange(boolean hasAgeRange) {
        this.hasAgeRange = hasAgeRange;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public boolean isHasBirthday() {
        return hasBirthday;
    }

    public void setHasBirthday(boolean hasBirthday) {
        this.hasBirthday = hasBirthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public boolean isHasGender() {
        return hasGender;
    }

    public void setHasGender(boolean hasGender) {
        this.hasGender = hasGender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
