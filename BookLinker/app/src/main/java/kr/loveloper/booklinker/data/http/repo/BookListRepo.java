package kr.loveloper.booklinker.data.http.repo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookListRepo {
    @SerializedName("meta")
    public MetaRepo meta;

    @SerializedName("documents")
    public List<BookRepo> books;

    @Override
    public String toString() {
        return "BookListRepo{" +
                "meta=" + meta +
                ", books=" + books +
                '}';
    }
}
