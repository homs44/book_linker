package kr.loveloper.booklinker.custom.webViewClient;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CustomWebViewClient extends WebViewClient {

    private CodeReceiver receiver;

    public CustomWebViewClient(CodeReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.contains("oauth?code=")) {
            String[] result = url.split("oauth\\?code=");
            receiver.onReceived(result[1]);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
    }
}
